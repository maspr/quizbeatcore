﻿namespace TestApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbWebServer = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbDbServer = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmdConnect = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbQuestions = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtUsrAuthID = new System.Windows.Forms.TextBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.txtGameID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtAnswer = new System.Windows.Forms.NumericUpDown();
            this.lnkAssignToGame = new System.Windows.Forms.LinkLabel();
            this.lnkPreheat = new System.Windows.Forms.LinkLabel();
            this.lnkInitCache = new System.Windows.Forms.LinkLabel();
            this.lnkCheckDB = new System.Windows.Forms.LinkLabel();
            this.lnkSendAnswer = new System.Windows.Forms.LinkLabel();
            this.lnkEndGame = new System.Windows.Forms.LinkLabel();
            this.lnkResetGame = new System.Windows.Forms.LinkLabel();
            this.lnkTest = new System.Windows.Forms.LinkLabel();
            this.lnkClearLog = new System.Windows.Forms.LinkLabel();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtLoginUsername = new System.Windows.Forms.TextBox();
            this.txtLoginPassword = new System.Windows.Forms.TextBox();
            this.txtLoginEmail = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lnkLogin = new System.Windows.Forms.LinkLabel();
            this.lnkRegister = new System.Windows.Forms.LinkLabel();
            this.txtLoginAuthToken = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAnswer)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbWebServer
            // 
            this.cmbWebServer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbWebServer.FormattingEnabled = true;
            this.cmbWebServer.Items.AddRange(new object[] {
            "https://quizbeatcore.conveyor.cloud",
            "https://quizbeatstaging.azurewebsites.net",
            "https://quizbe.at"});
            this.cmbWebServer.Location = new System.Drawing.Point(21, 40);
            this.cmbWebServer.Name = "cmbWebServer";
            this.cmbWebServer.Size = new System.Drawing.Size(286, 23);
            this.cmbWebServer.TabIndex = 0;
            this.cmbWebServer.Text = "https://quizbeatstaging.azurewebsites.net";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cmbDbServer);
            this.groupBox1.Controls.Add(this.cmbWebServer);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(495, 77);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Server info";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(369, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "DB Server";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Web Server";
            // 
            // cmbDbServer
            // 
            this.cmbDbServer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbDbServer.FormattingEnabled = true;
            this.cmbDbServer.Items.AddRange(new object[] {
            "QBStaging",
            "QBLocal",
            "QBLive"});
            this.cmbDbServer.Location = new System.Drawing.Point(312, 40);
            this.cmbDbServer.Name = "cmbDbServer";
            this.cmbDbServer.Size = new System.Drawing.Size(176, 23);
            this.cmbDbServer.TabIndex = 0;
            this.cmbDbServer.Text = "QBStaging";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmdConnect);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.cmbQuestions);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtUsrAuthID);
            this.groupBox2.Controls.Add(this.txtUserName);
            this.groupBox2.Controls.Add(this.txtGameID);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(3, 80);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(495, 112);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Parameters";
            // 
            // cmdConnect
            // 
            this.cmdConnect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdConnect.Location = new System.Drawing.Point(401, 16);
            this.cmdConnect.Name = "cmdConnect";
            this.cmdConnect.Size = new System.Drawing.Size(87, 25);
            this.cmdConnect.TabIndex = 0;
            this.cmdConnect.Text = "Connect";
            this.cmdConnect.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 80);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 17);
            this.label5.TabIndex = 1;
            this.label5.Text = "Questions";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(186, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 17);
            this.label6.TabIndex = 2;
            this.label6.Text = "AuthID";
            // 
            // cmbQuestions
            // 
            this.cmbQuestions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbQuestions.FormattingEnabled = true;
            this.cmbQuestions.Items.AddRange(new object[] {
            "QBStaging",
            "QBLocal",
            "QBLive"});
            this.cmbQuestions.Location = new System.Drawing.Point(85, 77);
            this.cmbQuestions.Name = "cmbQuestions";
            this.cmbQuestions.Size = new System.Drawing.Size(403, 23);
            this.cmbQuestions.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 17);
            this.label4.TabIndex = 2;
            this.label4.Text = "UserName";
            // 
            // txtUsrAuthID
            // 
            this.txtUsrAuthID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUsrAuthID.Location = new System.Drawing.Point(237, 46);
            this.txtUsrAuthID.Name = "txtUsrAuthID";
            this.txtUsrAuthID.Size = new System.Drawing.Size(251, 24);
            this.txtUsrAuthID.TabIndex = 1;
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(85, 46);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(92, 24);
            this.txtUserName.TabIndex = 1;
            // 
            // txtGameID
            // 
            this.txtGameID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGameID.Location = new System.Drawing.Point(85, 16);
            this.txtGameID.Name = "txtGameID";
            this.txtGameID.Size = new System.Drawing.Size(308, 24);
            this.txtGameID.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "GameID";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtAnswer);
            this.groupBox3.Controls.Add(this.lnkAssignToGame);
            this.groupBox3.Controls.Add(this.lnkPreheat);
            this.groupBox3.Controls.Add(this.lnkInitCache);
            this.groupBox3.Controls.Add(this.lnkCheckDB);
            this.groupBox3.Controls.Add(this.lnkSendAnswer);
            this.groupBox3.Controls.Add(this.lnkEndGame);
            this.groupBox3.Controls.Add(this.lnkResetGame);
            this.groupBox3.Controls.Add(this.lnkTest);
            this.groupBox3.Controls.Add(this.lnkClearLog);
            this.groupBox3.Controls.Add(this.txtLog);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 192);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(495, 433);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            // 
            // txtAnswer
            // 
            this.txtAnswer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAnswer.Location = new System.Drawing.Point(449, 38);
            this.txtAnswer.Maximum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.txtAnswer.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtAnswer.Name = "txtAnswer";
            this.txtAnswer.Size = new System.Drawing.Size(36, 24);
            this.txtAnswer.TabIndex = 5;
            this.txtAnswer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAnswer.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lnkAssignToGame
            // 
            this.lnkAssignToGame.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lnkAssignToGame.AutoSize = true;
            this.lnkAssignToGame.Location = new System.Drawing.Point(268, 40);
            this.lnkAssignToGame.Name = "lnkAssignToGame";
            this.lnkAssignToGame.Size = new System.Drawing.Size(95, 17);
            this.lnkAssignToGame.TabIndex = 4;
            this.lnkAssignToGame.TabStop = true;
            this.lnkAssignToGame.Text = "Assign to Game";
            // 
            // lnkPreheat
            // 
            this.lnkPreheat.AutoSize = true;
            this.lnkPreheat.Location = new System.Drawing.Point(84, 42);
            this.lnkPreheat.Name = "lnkPreheat";
            this.lnkPreheat.Size = new System.Drawing.Size(53, 17);
            this.lnkPreheat.TabIndex = 4;
            this.lnkPreheat.TabStop = true;
            this.lnkPreheat.Text = "Preheat";
            // 
            // lnkInitCache
            // 
            this.lnkInitCache.AutoSize = true;
            this.lnkInitCache.Location = new System.Drawing.Point(14, 42);
            this.lnkInitCache.Name = "lnkInitCache";
            this.lnkInitCache.Size = new System.Drawing.Size(64, 17);
            this.lnkInitCache.TabIndex = 4;
            this.lnkInitCache.TabStop = true;
            this.lnkInitCache.Text = "Init Cache";
            // 
            // lnkCheckDB
            // 
            this.lnkCheckDB.AutoSize = true;
            this.lnkCheckDB.Location = new System.Drawing.Point(125, 16);
            this.lnkCheckDB.Name = "lnkCheckDB";
            this.lnkCheckDB.Size = new System.Drawing.Size(61, 17);
            this.lnkCheckDB.TabIndex = 4;
            this.lnkCheckDB.TabStop = true;
            this.lnkCheckDB.Text = "Check DB";
            // 
            // lnkSendAnswer
            // 
            this.lnkSendAnswer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lnkSendAnswer.AutoSize = true;
            this.lnkSendAnswer.Location = new System.Drawing.Point(369, 40);
            this.lnkSendAnswer.Name = "lnkSendAnswer";
            this.lnkSendAnswer.Size = new System.Drawing.Size(80, 17);
            this.lnkSendAnswer.TabIndex = 4;
            this.lnkSendAnswer.TabStop = true;
            this.lnkSendAnswer.Text = "Send Answer";
            // 
            // lnkEndGame
            // 
            this.lnkEndGame.AutoSize = true;
            this.lnkEndGame.Location = new System.Drawing.Point(50, 15);
            this.lnkEndGame.Name = "lnkEndGame";
            this.lnkEndGame.Size = new System.Drawing.Size(66, 17);
            this.lnkEndGame.TabIndex = 4;
            this.lnkEndGame.TabStop = true;
            this.lnkEndGame.Text = "End Game";
            // 
            // lnkResetGame
            // 
            this.lnkResetGame.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lnkResetGame.AutoSize = true;
            this.lnkResetGame.Location = new System.Drawing.Point(372, 16);
            this.lnkResetGame.Name = "lnkResetGame";
            this.lnkResetGame.Size = new System.Drawing.Size(77, 17);
            this.lnkResetGame.TabIndex = 3;
            this.lnkResetGame.TabStop = true;
            this.lnkResetGame.Text = "Reset Game";
            // 
            // lnkTest
            // 
            this.lnkTest.AutoSize = true;
            this.lnkTest.Location = new System.Drawing.Point(12, 15);
            this.lnkTest.Name = "lnkTest";
            this.lnkTest.Size = new System.Drawing.Size(31, 17);
            this.lnkTest.TabIndex = 2;
            this.lnkTest.TabStop = true;
            this.lnkTest.Text = "Test";
            // 
            // lnkClearLog
            // 
            this.lnkClearLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lnkClearLog.AutoSize = true;
            this.lnkClearLog.Location = new System.Drawing.Point(452, 16);
            this.lnkClearLog.Name = "lnkClearLog";
            this.lnkClearLog.Size = new System.Drawing.Size(37, 17);
            this.lnkClearLog.TabIndex = 2;
            this.lnkClearLog.TabStop = true;
            this.lnkClearLog.Text = "Clear";
            this.lnkClearLog.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkClearLog_LinkClicked);
            // 
            // txtLog
            // 
            this.txtLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLog.BackColor = System.Drawing.Color.Black;
            this.txtLog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLog.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.txtLog.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtLog.Location = new System.Drawing.Point(5, 68);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtLog.Size = new System.Drawing.Size(485, 359);
            this.txtLog.TabIndex = 1;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(509, 656);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(501, 628);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Parameters";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(501, 628);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Test";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtLoginAuthToken);
            this.groupBox4.Controls.Add(this.lnkRegister);
            this.groupBox4.Controls.Add(this.lnkLogin);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.txtLoginPassword);
            this.groupBox4.Controls.Add(this.txtLoginEmail);
            this.groupBox4.Controls.Add(this.txtLoginUsername);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(3, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(495, 113);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Auth";
            // 
            // groupBox5
            // 
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox5.Location = new System.Drawing.Point(3, 116);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(495, 125);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            // 
            // txtLoginUsername
            // 
            this.txtLoginUsername.Location = new System.Drawing.Point(92, 49);
            this.txtLoginUsername.Name = "txtLoginUsername";
            this.txtLoginUsername.Size = new System.Drawing.Size(190, 24);
            this.txtLoginUsername.TabIndex = 0;
            // 
            // txtLoginPassword
            // 
            this.txtLoginPassword.Location = new System.Drawing.Point(92, 79);
            this.txtLoginPassword.Name = "txtLoginPassword";
            this.txtLoginPassword.Size = new System.Drawing.Size(190, 24);
            this.txtLoginPassword.TabIndex = 0;
            // 
            // txtLoginEmail
            // 
            this.txtLoginEmail.Location = new System.Drawing.Point(92, 19);
            this.txtLoginEmail.Name = "txtLoginEmail";
            this.txtLoginEmail.Size = new System.Drawing.Size(190, 24);
            this.txtLoginEmail.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 17);
            this.label7.TabIndex = 1;
            this.label7.Text = "Email";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(20, 52);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 17);
            this.label8.TabIndex = 1;
            this.label8.Text = "Username";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(20, 82);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 17);
            this.label9.TabIndex = 1;
            this.label9.Text = "Password";
            // 
            // lnkLogin
            // 
            this.lnkLogin.AutoSize = true;
            this.lnkLogin.Location = new System.Drawing.Point(299, 26);
            this.lnkLogin.Name = "lnkLogin";
            this.lnkLogin.Size = new System.Drawing.Size(38, 17);
            this.lnkLogin.TabIndex = 2;
            this.lnkLogin.TabStop = true;
            this.lnkLogin.Text = "Login";
            // 
            // lnkRegister
            // 
            this.lnkRegister.AutoSize = true;
            this.lnkRegister.Location = new System.Drawing.Point(299, 52);
            this.lnkRegister.Name = "lnkRegister";
            this.lnkRegister.Size = new System.Drawing.Size(55, 17);
            this.lnkRegister.TabIndex = 2;
            this.lnkRegister.TabStop = true;
            this.lnkRegister.Text = "Register";
            // 
            // txtLoginAuthToken
            // 
            this.txtLoginAuthToken.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLoginAuthToken.Location = new System.Drawing.Point(288, 79);
            this.txtLoginAuthToken.Name = "txtLoginAuthToken";
            this.txtLoginAuthToken.Size = new System.Drawing.Size(203, 24);
            this.txtLoginAuthToken.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 656);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Calibri", 10F);
            this.Name = "Form1";
            this.Text = "Tester";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAnswer)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbWebServer;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbDbServer;
        private System.Windows.Forms.TextBox txtGameID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Button cmdConnect;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtUsrAuthID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbQuestions;
        private System.Windows.Forms.TextBox txtLog;
        private System.Windows.Forms.LinkLabel lnkClearLog;
        private System.Windows.Forms.LinkLabel lnkTest;
        private System.Windows.Forms.LinkLabel lnkResetGame;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.LinkLabel lnkEndGame;
        private System.Windows.Forms.LinkLabel lnkSendAnswer;
        private System.Windows.Forms.NumericUpDown txtAnswer;
        private System.Windows.Forms.LinkLabel lnkAssignToGame;
        private System.Windows.Forms.LinkLabel lnkCheckDB;
        private System.Windows.Forms.LinkLabel lnkInitCache;
        private System.Windows.Forms.LinkLabel lnkPreheat;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtLoginPassword;
        private System.Windows.Forms.TextBox txtLoginEmail;
        private System.Windows.Forms.TextBox txtLoginUsername;
        private System.Windows.Forms.LinkLabel lnkRegister;
        private System.Windows.Forms.LinkLabel lnkLogin;
        private System.Windows.Forms.TextBox txtLoginAuthToken;
    }
}

