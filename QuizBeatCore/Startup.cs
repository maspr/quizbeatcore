﻿using Core.AspNet;
using Core.AspNet.Services;
using Core.Common;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using QuizBeatCore.Services;
using Serilog;
using System;

namespace QuizBeatCore
{
    public class Startup
    {
        public Startup(IHostingEnvironment env, IConfiguration configuration)
        {
            try
            {
                Configuration = configuration;

                var builder = new ConfigurationBuilder()
                    .SetBasePath(env.ContentRootPath)
                    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                    .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                    .AddEnvironmentVariables();

                var logger = new LoggerConfiguration().WriteTo.File("..\\logs\\QBLog.txt", rollingInterval: RollingInterval.Day, shared: true).CreateLogger();

                Configuration = builder.Build();
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);
            }

        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            try
            {
                //@@@
                //Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "Development");
                //@@@

                // Add framework services.
                services.AddMemoryCache();
                services.AddSession(options =>
                {
                    options.IdleTimeout = TimeSpan.FromMinutes(60);
                    options.Cookie.Name = ".QB";
                });

                services.AddResponseCompression();

                //services.Configure<AppSettings>(options => Configuration.GetSection("AppSettings").Bind(options));
                //services.Configure<QuizBeatAppSettings>(options => Configuration.GetSection("AppSettings").Bind(options));
                //ConfigHelper.Configure(Configuration.GetSection("AppSettings").Get<AppSettings>());
                //QuizBeatConfigHelper.Configure(Configuration.GetSection("AppSettings").Get<QuizBeatAppSettings>());

                ConfigHelper.Configuration = Configuration;

                //services.AddDbContext<QuizBeatContext>(options => options.UseSqlServer(Configuration.GetConnectionString("QuizBeatStaging")));  // ConfigHelper.Current.ConnectionStrings[ConfigHelper.Current.ActiveConnStr]

                services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

                //services.AddLogging();
                services.AddTransient<IUrlHelperFactory, UrlHelperFactory>();

                //services.AddTransient<ISessionService, SessionService>();
                services.AddTransient<ILookupService, LookupService>();
                services.AddTransient<IImageStorageService, GoogleImageStorageSvc>();
                services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
                services.AddSingleton<ICacheService, CacheService>();
                services.AddSingleton<IDbCacheService, DbCacheService>();

                //IFileProvider embeddedProvider = new EmbeddedFileProvider(System.Reflection.Assembly.GetEntryAssembly());
                //services.AddSingleton<IFileProvider>(embeddedProvider);

                services.AddTransient<IQbDataService, QbDataService>();
                services.AddTransient<IContextService, ContextService>();
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);
            }

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            try
            {
                //loggerFactory.AddConsole(Configuration.GetSection("Logging"));
                //loggerFactory.AddDebug();


                if (env.IsDevelopment())
                {
                    app.UseDeveloperExceptionPage();
                }
                else
                {
                    app.UseHsts();
                }

                HttpHelper.Configure(app.ApplicationServices.GetRequiredService<IHttpContextAccessor>());

                SessionOptions sop = new SessionOptions() { IdleTimeout = TimeSpan.FromMinutes(60) };
                sop.Cookie.Name = ".QB";
                app.UseSession(sop);
                app.UseStaticFiles(new StaticFileOptions()
                {
                    OnPrepareResponse = context =>
                    {
                        context.Context.Response.Headers.Add("Cache-Control", "no-cache, no-store");
                        context.Context.Response.Headers.Add("Expires", "-1");
                    }
                });
                app.UseStatusCodePagesWithReExecute("/StatusCode/{0}");

                app.UseMvcWithDefaultRoute();

                app.UseResponseCompression();

                app.UseResponseBuffering();

                app.UseHttpsRedirection();
                app.UseMvc(routes =>
                {
                    routes.MapRoute(
                        name: "default",
                        template: "{controller=Home}/{action=Index}/{id?}");
                });

                AppHttpContext.Services = app.ApplicationServices;
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);
            }
        }
    }

}
