﻿using QuizBeat.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json.Serialization;

namespace QuizBeat.Models.Dto
{
    [Serializable]
    public class UserDto
    {
        public UserDto(User user)
        {
            this.ID = user.ID;
            this.Username = user.Username;
            this.Money_Won = user.Money_Won;
            this.ExtraLives = user.ExtraLives;
            this.Money_CashedOut = user.Money_CashedOut;
            this.Ranking = user.Ranking;
            this.HasSharedAppOnFacebook = user.HasSharedAppOnFacebook;
            this.HasSharedAppOnTwitter = user.HasSharedAppOnTwitter;

        }

        public static List<UserDto> ConvertToDto(IEnumerable<User> users)
        {
            List<UserDto> userDtos = new List<UserDto>();
            foreach (User user in users)
            {
                userDtos.Add(new UserDto(user));
            }
            return userDtos;
        }

        [JsonProperty(PropertyName = "ID")]
        public string ID { get; set; }

        [JsonProperty(PropertyName = "Username")]
        public string Username { get; set; }

        [JsonProperty(PropertyName = "Email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "Money_Won")]
        public double? Money_Won { get; set; }

        [JsonProperty(PropertyName = "Money_CashedOut")]
        public double? Money_CashedOut { get; set; }

        [JsonProperty(PropertyName = "ExtraLives")]
        public int? ExtraLives { get; set; }

        [JsonProperty(PropertyName = "Ranking")]
        public int? Ranking { get; set; }

        [JsonProperty(PropertyName = "HasSharedAppOnFacebook")]
        public bool? HasSharedAppOnFacebook { get; set; }

        [JsonProperty(PropertyName = "HasSharedAppOnTwitter")]
        public bool? HasSharedAppOnTwitter { get; set; }

        public override string ToString()
        {
            return $"Username: {Username}|Email: {Email}|Money_Won: {Money_Won}|Money_CashedOut: {Money_CashedOut}|ExtraLives: {ExtraLives}|Ranking: {Ranking}";
        }

    }
    public class Prize_Won
    {
        public string Name { get; set; } = "€";
        public double? Amount { get; set; } = 0;
        public string Type { get; set; } = "cash";
    }


    [Serializable]
    public class UserShortDto
    {
        public UserShortDto(User user)
        {
            this.ID = user.ID;
            this.Username = user.Username;
            this.Money_Won = user.Money_Won;
            this.Ranking = user.Ranking;
            this.Prize_Won = "<null>";  // this.Money_Won.Value.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture) + "€";
        
            if (this.Money_Won == null) this.Money_Won = 0;
            if (this.Ranking == null) this.Ranking = 0;
        }

        public static List<UserShortDto> ConvertToDto(List<User> users)
        {
            List<UserShortDto> userDtos = new List<UserShortDto>();
            for (int i = 0; i < users.Count(); i++)
            {
                User userTemp = (User)users[i];
                UserShortDto shortDto = new UserShortDto(userTemp);
                userDtos.Add(shortDto);
            }
            return userDtos;
        }
        public static List<UserShortDto> ConvertToDto(IEnumerable<User> users)
        {
            List<UserShortDto> userDtos = new List<UserShortDto>();

            foreach (var u in users)
            {
                userDtos.Add(new UserShortDto(u));
            }
            return userDtos;
        }
        [JsonProperty(PropertyName = "ID")]
        public string ID { get; set; }

        [JsonProperty(PropertyName = "Username")]
        public string Username { get; set; }

        [JsonProperty(PropertyName = "Prize_Won")]
        public string Prize_Won { get; set; }

        [JsonProperty(PropertyName = "Money_Won")]
        public double? Money_Won { get; set; } = 0;

        [JsonProperty(PropertyName = "Ranking")]
        public int? Ranking { get; set; } = 0;

        public override string ToString()
        {
            return $"Username: {Username}|Prize_Won: {Prize_Won}|Money_Won: {Money_Won}|Ranking: {Ranking}";
        }
    }



    [Serializable]
    public class UserShortDtoTop100
    {
        public UserShortDtoTop100(User user)
        {
            this.ID = user.ID;
            this.Username = user.Username;
            this.Money_Won = user.Money_Won;
            this.Ranking = user.Ranking;
            //this.Prize_Won = "<null>";  // this.Money_Won.Value.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture) + "€";

            if (this.Money_Won == null) this.Money_Won = 0;
            if (this.Ranking == null) this.Ranking = 0;
        }

        public static List<UserShortDtoTop100> ConvertToDto(List<User> users)
        {
            List<UserShortDtoTop100> userDtos = new List<UserShortDtoTop100>();
            for (int i = 0; i < users.Count(); i++)
            {
                User userTemp = (User)users[i];
                UserShortDtoTop100 shortDto = new UserShortDtoTop100(userTemp);
                userDtos.Add(shortDto);
            }
            return userDtos;
        }
        public static List<UserShortDtoTop100> ConvertToDto(IEnumerable<User> users)
        {
            List<UserShortDtoTop100> userDtos = new List<UserShortDtoTop100>();

            foreach (var u in users)
            {
                userDtos.Add(new UserShortDtoTop100(u));
            }
            return userDtos;
        }
        [JsonProperty(PropertyName = "ID")]
        public string ID { get; set; }

        [JsonProperty(PropertyName = "Username")]
        public string Username { get; set; }

        //[JsonProperty(PropertyName = "Prize_Won")]
        //public string Prize_Won { get; set; }

        [JsonProperty(PropertyName = "Money_Won")]
        public double? Money_Won { get; set; } = 0;

        [JsonProperty(PropertyName = "Ranking")]
        public int? Ranking { get; set; } = 0;

        public override string ToString()
        {
            return $"Username: {Username}|Money_Won: {Money_Won}|Ranking: {Ranking}";
        }
    }


}