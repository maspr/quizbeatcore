using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace QuizBeat.Models.Database
{

    [Table("GameRating")]
    [Serializable]
    public partial class GameRating
    {
        [StringLength(40)]
        public string ID { get; set; }

        [StringLength(40)]
        public string GameID { get; set; }

        [StringLength(40)]
        public string UserID { get; set; }

        public int? Rating { get; set; }
    }
}
