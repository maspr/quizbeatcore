using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace QuizBeat.Models.Database
{

    [Table("Game")]
    [Serializable]
    public partial class Game
    {
        public Game()
        {
            Questions = new List<Question>();
            User_Game = new List<User_Game>();
        }

        public DateTime? StartDateTime { get; set; }

        public bool? IsCompleted { get; set; }

        [StringLength(40)]
        public string PrizeID { get; set; }

        public bool? IsActive { get; set; }

        [StringLength(40)]
        public string ID { get; set; }

        public bool? HasStarted { get; set; }

        public DateTime? TimeStarted { get; set; }

        public int? NumberOfQuestions { get; set; }

        public bool? PushAutoReminder { get; set; }

        public bool? FastestWinner { get; set; }

        public virtual Prize Prize { get; set; }

        public virtual IList<Question> Questions { get; set; } = new List<Question>();

        public virtual IList<User_Game> User_Game { get; set; }

        public override string ToString()
        {
            return $"{ID}-IsActive:{IsActive}-HasStarted:{HasStarted}-IsCompleted:{IsCompleted}-TimeStarted:{TimeStarted}";
        }

    }
}
