using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace QuizBeat.Models.Database
{

    [Table("LivesUsed")]
    [Serializable]
    public partial class LivesUsed
    {
        [StringLength(40)]
        public string ID { get; set; }

        public int? Count { get; set; }

        [StringLength(40)]
        public string QuestionID { get; set; }

        [StringLength(40)]
        public string User_QuestionID { get; set; }

        public DateTime? DateCreated { get; set; }

        [StringLength(40)]
        public string UserID { get; set; }
    }
}
