using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace QuizBeat.Models.Database
{

    [Table("LivesBought")]
    [Serializable]
    public partial class LivesBought
    {
        [StringLength(40)]
        public string ID { get; set; }

        public DateTime? DateCreated { get; set; }

        public int? NumberOfLives { get; set; }

        [StringLength(40)]
        public string UserID { get; set; }

        public string ReceiptData { get; set; }
    }
}
