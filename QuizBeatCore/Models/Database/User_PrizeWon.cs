using System;
using System.ComponentModel.DataAnnotations;

namespace QuizBeat.Models.Database
{
    [Serializable]
    public partial class User_PrizeWon
    {
        [Required]
        [StringLength(40)]
        public string UserID { get; set; }

        [Required]
        [StringLength(40)]
        public string PrizeID { get; set; }

        public double Amount { get; set; }

        public DateTime? DateTime { get; set; }

        [Required]
        [StringLength(40)]
        public string GameID { get; set; }

        [StringLength(40)]
        public string ID { get; set; }

        public virtual Prize Prize { get; set; }
    }
}
