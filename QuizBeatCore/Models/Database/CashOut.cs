    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

namespace QuizBeat.Models.Database
{

    [Table("CashOut")]
    [Serializable]
    public partial class CashOut
    {
        [StringLength(40)]
        public string ID { get; set; }

        [StringLength(40)]
        public string UserID { get; set; }

        public double? Money { get; set; }

        public DateTime? Date { get; set; }

        public bool? IsApproved { get; set; }

        [StringLength(40)]
        public string IBAN { get; set; }

        [StringLength(140)]
        public string FullName { get; set; }

        [StringLength(20)]
        public string Type { get; set; }
    }
}
