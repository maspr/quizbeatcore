    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

namespace QuizBeat.Models.Database
{

    [Table("Cache")]
    [Serializable]
    public partial class Cache
    {
        [StringLength(40)]
        public string ID { get; set; }

        [StringLength(50)]
        public string Key { get; set; }

        public string Value { get; set; }

        public DateTime? DateCreated { get; set; }

        public DateTime? DateUpdated { get; set; }

    }
}
