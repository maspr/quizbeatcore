﻿using Newtonsoft.Json;
using QuizBeat.Models.Database;
using System;

namespace QuizBeat.Models.Dto
{
    [Serializable]
    public class UserOption
    {
        public User ConvertToUser()
        {
            User user = new User();
            user.ID = this.ID;
            user.Email = this.Email;
            user.Username = this.Username;
            user.Money_Won = this.Money_Won;
            user.ExtraLives = this.ExtraLives;
            user.Password = this.Password;
            user.AuthToken = new Guid(this.AuthToken);
            user.Created_At = DateTime.UtcNow;
            user.Ranking = this.RankIndex;
            return user;
        }

        private string _email = string.Empty;
        [JsonProperty(PropertyName = "Email")]
        public string Email { get { return string.IsNullOrEmpty(_email) ? string.Empty : _email.Trim(); } set { _email = string.IsNullOrEmpty(value) ? string.Empty : value.Trim(); } }

        [JsonProperty(PropertyName = "Password")]
        public string Password { get; set; }

        private string _username = string.Empty;
        [JsonProperty(PropertyName = "Username")]
        public string Username { get { return string.IsNullOrEmpty(_username) ? string.Empty : _username.Trim(); } set { _username = string.IsNullOrEmpty(value) ? string.Empty : value.Trim(); } }

        [JsonProperty(PropertyName = "Money_Won")]
        public double Money_Won { get; set; }

        [JsonProperty(PropertyName = "ExtraLives")]
        public int ExtraLives { get; set; }

        [JsonProperty(PropertyName = "AuthToken")]
        public string AuthToken { get; set; }

        [JsonProperty(PropertyName = "InvitationCode")]
        public string InvitationCode { get; set; }

        [JsonProperty(PropertyName = "ID")]
        public string ID { get; set; }

        [JsonProperty(PropertyName = "RankIndex")]
        public int? RankIndex { get; set; }

        [JsonProperty(PropertyName = "HashString")]
        public string HashString { get; set; }

    }
}