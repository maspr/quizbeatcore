﻿function NextQuestion() {
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "http://quizbeat.azurewebsites.net/api/camera/nextquestion", true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
    var response = JSON.parse(xhttp.responseText);
}

function StartGame() {
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "http://quizbeat.azurewebsites.net/api/camera/StartGame", true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
    var response = JSON.parse(xhttp.responseText);
}


function EndGame() {
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "http://quizbeat.azurewebsites.net/api/camera/EndGame", true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
    var response = JSON.parse(xhttp.responseText);
}


function ResetPassword() {
    var value1 = document.getElementById("Text1").value;
    var value2 = document.getElementById("Text2").value;
    var tokenValue = getQueryStringValue('ResetPasswordToken');

    if (value1 != value2) {
        document.getElementById("resp3").style["display"] = "block";
        return;
    }
    document.getElementById("resp3").style["display"] = "none";
    var postString = "https://quizbeat.azurewebsites.net/api/User/resetPasswordWithToken";

    var data0 = { password: value1, token: tokenValue };
    $("#resetButton").prop("disabled", true);

    $.ajax({
        type: 'GET',
        url: "https://quizbeat.azurewebsites.net/api/User/resetPasswordWithToken",
        data: data0,
        dataType: 'json',
        contentType:"application/json",
        success: function (data) {
            document.getElementById("resp1").style["display"] = "block";
            $("#resetButton").prop("disabled", false);
        },
        error: function (result) {
            document.getElementById("resp2").style["display"] = "block";
            $("#resetButton").prop("disabled", false);
        } 
    });

}


function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}  