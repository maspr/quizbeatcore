﻿using Core.AspNet.Services;
using Core.Common;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using QuizBeat.Models.Database;
using QuizBeatCore.Services;
using Serilog;
using System;
using System.Threading.Tasks;

namespace QuizBeat.Controllers
{
    public class BaseController : Controller
    {

        internal IContextService ctxSvc;
        internal IHostingEnvironment hostEnv;
        internal Guid authorization = Guid.Empty;
        internal IDbCacheService cacheSvc = null;
        internal IConfiguration Config;
        internal IQbDataService dbSvc = null;
        internal const string ADMIN_AUTH_TOKEN = "220f78a7-7dcc-49a5-b581-e2038c5efc81";

        public BaseController(IContextService ctx, IHostingEnvironment env, IConfiguration c, IQbDataService da, IDbCacheService dbc)
        {
            if (env != null) hostEnv = env;
            if (ctx != null) ctxSvc = ctx;
            if (c != null) Config = c;
            if (dbc != null) cacheSvc = dbc;
            if (da != null) dbSvc = da;

            authorization = GetAuthorization();
            if (cacheSvc != null) cacheSvc.Mode = enCacheMode.Memory;

        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);

            if (Request != null)
            {
                string path = Request.Path;

            }
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {

            base.OnActionExecuted(context);
            if (Request != null)
            {
                string path = Request.Path;
            }
        }

        #region < LOG >
        internal void LogError(Exception ex, string msg, object[] args)
        {
            Log.Error(ex, msg);
            //Log.CloseAndFlush();
        }
        internal void LogError(string msg)
        {
            Log.Error(msg);
            //Log.CloseAndFlush();
        }
        internal void LogInfo(string msg)
        {
            Log.Information(msg);
            //Log.CloseAndFlush();
        }
        #endregion

        internal Guid GetAuthorization()
        {
            if (authorization != Guid.Empty) return authorization;
            if (Request != null && Request.Headers != null && Request.Headers.Keys.Contains("Authorization"))
            {
                authorization = new Guid(Request.Headers["Authorization"]);
            }
            else
            {
                authorization = Guid.Empty;
            }
            return authorization;
        }

        #region < Get User >
        internal async Task<User> GetUser(string email = null, string username = null, Guid authToken = default(Guid), string id = null, bool fromCache = false)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await GetUser(da, email, username, authToken, id, fromCache);
            }
        }

        internal async Task<User> GetUser(DataSvc da, string email = null, string username = null, Guid authToken = default(Guid), string id = null, bool fromCache = false)
        {
            User usr = null;

            if (!fromCache)
            {
                if (!email.IsNullOrEmpty())
                {
                    usr = await da.GetUserByEmail(email);
                }
                else if (!username.IsNullOrEmpty())
                {
                    usr = await da.GetUserByUserName(username);
                }
                else if (authToken != default(Guid))
                {
                    usr = await da.GetUserByAuthToken(authToken.AsString());
                }
                else if (!id.IsNullOrEmpty())
                {
                    usr = await da.GetUserByID(id);
                }
            }
            if (fromCache)
            {

                if (!email.IsNullOrEmpty())
                {
                    usr = await cacheSvc.Get<User>(da, $"USR_{email}");
                    if (usr == null)
                    {
                        usr = await da.GetUserByEmail(email);
                        if (usr != null)
                        {
                            await SetUser(da, usr);
                        }
                    }
                }
                else if (!username.IsNullOrEmpty())
                {
                    usr = await cacheSvc.Get<User>(da, $"USR_{username}");
                    if (usr == null)
                    {
                        usr = await da.GetUserByUserName(username);
                        if (usr != null)
                        {
                            await SetUser(da, usr);
                        }
                    }
                }
                else if (authToken != default(Guid))
                {
                    usr = await cacheSvc.Get<User>(da, $"USR_{authToken}");
                    if (usr == null)
                    {
                        usr = await da.GetUserByAuthToken(authToken.AsString());
                        if (usr != null)
                        {
                            await SetUser(da, usr);
                        }
                    }
                }
                else if (!id.IsNullOrEmpty())
                {
                    usr = await cacheSvc.Get<User>(da, $"USR_{id}");
                    if (usr == null)
                    {
                        usr = await da.GetUserByID(id);
                        if (usr != null)
                        {
                            await SetUser(da, usr);
                        }
                    }
                }
            }
            return usr;
        }
        #endregion


        #region < SetUser >
        internal async Task<User> SetUser(DataSvc da, User usr)
        {
            if (usr.AuthToken == null || usr.AuthToken == Guid.Empty) GetAuthorization();
            if (da != null)
            {
                await cacheSvc.Set<User>(da, $"USR_{usr.Email}", usr, new TimeSpan(2, 0, 0));
                await cacheSvc.Set<User>(da, $"USR_{usr.Username}", usr, new TimeSpan(2, 0, 0));
                await cacheSvc.Set<User>(da, $"USR_{usr.ID}", usr, new TimeSpan(2, 0, 0));
                await cacheSvc.Set<User>(da, $"USR_{usr.AuthToken ?? authorization}", usr, new TimeSpan(2, 0, 0));
            }
            else
            {
                using (DataSvc dal = DataSvc.GetDataService())
                {
                    await cacheSvc.Set<User>(dal, $"USR_{usr.Email}", usr, new TimeSpan(2, 0, 0));
                    await cacheSvc.Set<User>(dal, $"USR_{usr.Username}", usr, new TimeSpan(2, 0, 0));
                    await cacheSvc.Set<User>(dal, $"USR_{usr.ID}", usr, new TimeSpan(2, 0, 0));
                    await cacheSvc.Set<User>(dal, $"USR_{usr.AuthToken ?? authorization}", usr, new TimeSpan(2, 0, 0));
                }
            }
            return usr;
        }
        #endregion

        internal async Task<Game> SetGame(Game game, DataSvc da = null)
        {
            if (da != null)
            {
                await cacheSvc.Set<Game>(da, "ACTIVE_GAME", game, new TimeSpan(2, 0, 0));
                await cacheSvc.Set<Game>(da, "STARTED_GAME", game, new TimeSpan(2, 0, 0));
                await cacheSvc.Set<Game>(da, "RUNNING_GAME", game, new TimeSpan(2, 0, 0));
            }
            else
            {
                using (DataSvc dal = DataSvc.GetDataService())
                {
                    await cacheSvc.Set<Game>(dal, "ACTIVE_GAME", game, new TimeSpan(2, 0, 0));
                    await cacheSvc.Set<Game>(dal, "STARTED_GAME", game, new TimeSpan(2, 0, 0));
                    await cacheSvc.Set<Game>(dal, "RUNNING_GAME", game, new TimeSpan(2, 0, 0));
                }

            }
            return game;
        }

        internal async Task<Game> SetGame(string key, Game game, DataSvc da = null)
        {
            if (da != null)
            {
                await cacheSvc.Set<Game>(da, key, game, new TimeSpan(2, 0, 0));
            }
            else
            {
                using (DataSvc dal = DataSvc.GetDataService())
                {
                    await cacheSvc.Set<Game>(dal, key, game, new TimeSpan(2, 0, 0));
                }
            }
            return game;
        }

        #region < GetActiveGame >
        internal async Task<Game> GetActiveGame(bool? includeQuestions = null, bool? includePrize = null)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await GetActiveGame(da, includeQuestions, includePrize);
            }
        }

        internal async Task<Game> GetActiveGame(DataSvc da, bool? includeQuestions = null, bool? includePrize = null)
        {
            Game game = null;
            bool incQuestions = includeQuestions.HasValue && includeQuestions == true;
            bool incPrize = includePrize.HasValue && includePrize == true;

            game = await cacheSvc.Get<Game>(da, "ACTIVE_GAME");
            if (game == null || game.Questions.Count == 0 || game.Prize == null)
            {
                game = await da.FindActiveGame(true, true);
                if (game != null)
                {
                    await cacheSvc.Set<Game>(da, "ACTIVE_GAME", game, new TimeSpan(2, 0, 0));
                }
            }

            if (incQuestions && incPrize)
                return game;

            if (!incQuestions && incPrize)
                if (game != null && game.Questions != null) game.Questions.Clear();

            if (!incQuestions && !incPrize)
            {
                if (game != null && game.Questions != null)
                {
                    game.Questions.Clear();
                }
            }
            return game;
        }
        #endregion


        #region < GetRunningGame >
        internal async Task<Game> GetRunningGame(bool? includeQuestions = null, bool? includePrize = null)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await GetRunningGame(da, includeQuestions, includePrize);
            }
        }

        internal async Task<Game> GetRunningGame(DataSvc da, bool? includeQuestions = null, bool? includePrize = null)
        {
            Game game = null;
            bool incQuestions = includeQuestions.HasValue && includeQuestions == true;
            bool incPrize = includePrize.HasValue && includePrize == true;

            game = await cacheSvc.Get<Game>(da, "RUNNING_GAME");
            if (game == null || game.Questions.Count == 0 || game.Prize == null)
            {
                game = await da.FindRunningGame(true, true);
                if (game != null)
                {
                    await cacheSvc.Set<Game>(da, "RUNNING_GAME", game, new TimeSpan(2, 0, 0));
                }
            }
            if (game != null)
            {
                if (incQuestions && incPrize)
                    return game;

                if (!incQuestions && incPrize)
                    game.Questions.Clear();

                if (!incQuestions && !incPrize)
                {
                    game.Questions.Clear();
                }
            }
            return game;
        }
        #endregion


        #region < GetStartedGame >
        internal async Task<Game> GetStartedGame()
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await GetStartedGame(da);
            }
        }

        internal async Task<Game> GetStartedGame(DataSvc da)
        {
            Game game = null;
            game = await cacheSvc.Get<Game>(da, "STARTED_GAME");
            if (game == null || game.Questions.Count == 0)
            {
                game = await da.FindStartedgGame();
                if (game != null)
                {
                    await cacheSvc.Set<Game>(da, "STARTED_GAME", game, new TimeSpan(2, 0, 0));
                }
            }
            return game;
        }
        #endregion


        #region < GetNextGame >
        internal async Task<Game> GetNextGame()
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await GetNextGame(da);
            }
        }

        internal async Task<Game> GetNextGame(DataSvc da)
        {
            Game game = await cacheSvc.Get<Game>(da, "NEXTGAME");
            if (game == null || game.Questions.Count == 0)
            {
                game = await da.FindNextGame();
                if (game != null)
                {
                    await cacheSvc.Set<Game>(da, "NEXTGAME", game, new TimeSpan(2, 0, 0));
                }
            }
            return game;
        }
        #endregion


        #region < GetUserGame >
        internal async Task<User_Game> GetUserGame(string userID = null, string gameID = null)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await GetUserGame(da, userID, gameID);
            }
        }

        internal async Task<User_Game> GetUserGame(DataSvc da, string userID = null, string gameID = null)
        {
            User_Game ug = null;
            if (!userID.IsNullOrEmpty() && !gameID.IsNullOrEmpty())
            {
                ug = await cacheSvc.Get<User_Game>(da, $"UG_{userID}_{gameID}");
                if (ug == null || ug.User_Question.Count == 0)
                {
                    ug = await da.FindUser_Game(userID, gameID);
                    if (ug != null) await SetUserGame(da, ug);
                }
            }
            else if (!userID.IsNullOrEmpty() && gameID.IsNullOrEmpty())
            {
                ug = await cacheSvc.Get<User_Game>(da, $"UG_{userID}");
                if (ug == null || ug.User_Question.Count == 0)
                {
                    ug = await da.FindUser_GameByUserID(userID);
                    if (ug != null) await SetUserGame(da, ug);
                }
            }
            return ug;
        }
        #endregion


        #region < Set/Reset User game >
        internal async Task SetUserGame(DataSvc da, User_Game ug)
        {
            if (!ug.UserID.IsNullOrEmpty() && !ug.GameID.IsNullOrEmpty())
            {
                await cacheSvc.Set<User_Game>(da, $"UG_{ug.UserID}_{ug.GameID}", ug);
            }
            else if (!ug.UserID.IsNullOrEmpty() && ug.GameID.IsNullOrEmpty())
            {
                await cacheSvc.Set<User_Game>(da, $"UG_{ug.UserID}", ug);
            }
        }

        internal async Task ResetUserGame(User_Game ug)
        {
            await cacheSvc.Set<User_Game>($"UG_{ug.UserID}_{ug.GameID}", null);
            await cacheSvc.Set<User_Game>($"UG_{ug.UserID}", null);
        }

        internal async Task ResetUserGame(DataSvc da, User_Game ug)
        {
            await cacheSvc.Set<User_Game>(da, $"UG_{ug.UserID}_{ug.GameID}", null);
            await cacheSvc.Set<User_Game>(da, $"UG_{ug.UserID}", null);
        }

        #endregion


        #region < GetDonation >
        internal async Task<Donation> GetDonation()
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await GetDonation(da);
            }
        }

        internal async Task<Donation> GetDonation(DataSvc da)
        {
            Donation donation = await cacheSvc.Get<Donation>(da, "DONATION");
            if (donation == null)
            {
                donation = await da.FindDonation(isActive: true);
                if (donation != null)
                {
                    await cacheSvc.Set<Donation>(da, "DONATION", donation, new TimeSpan(2, 0, 0));
                }
            }
            return donation;
        }
        #endregion


        internal async Task ClearCacheAsync(DataSvc da)
        {
            //var result = this.cacheSvc.Select(t => new
            //{
            //    Key = t.Key,
            //    Value = t.Value
            //}).ToArray();

            await Task.Run(() => cacheSvc.Clear(da));
            await cacheSvc.Clear(da);
        }


        internal async Task ClearCache(DataSvc da)
        {
            await Task.Run(() => cacheSvc.Clear(da));
            await cacheSvc.Clear(da);
        }

    }

    public class ResponseMessage
    {
        [JsonProperty(PropertyName = "Message")]
        public string Message { get; set; } = "";
    }

}
