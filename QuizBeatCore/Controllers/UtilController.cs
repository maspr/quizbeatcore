﻿using Core.AspNet.Services;
using Core.Common;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using QuizBeat.Controllers;
using QuizBeat.Models.Database;
using QuizBeatCore.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace QuizBeatCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UtilController : BaseController
    {

        public UtilController(IContextService ctx, IHostingEnvironment env, IConfiguration c, IQbDataService da, IDbCacheService dbc)
            : base(ctx, env, c, da, dbc)
        {

        }

        // GET api/values
        [HttpGet("PreheatGame")]
        public async Task<ActionResult> PreheatGame()
        {

            if (Request.Headers.ContainsKey("AuthToken"))
            {
                if (Request.Headers["AuthToken"] != ADMIN_AUTH_TOKEN)
                {
                    return NotFound();
                }

                try
                {
                    int ret = 0;
                    using (DataSvc da = DataSvc.GetDataService())
                    {
                        ret = await da.Warmup();
                    }

                    int availableWorker, availableIO;
                    int maxWorker, maxIO;

                    ThreadPool.GetAvailableThreads(out availableWorker, out availableIO);
                    ThreadPool.GetMaxThreads(out maxWorker, out maxIO);

                    return Ok(new
                    {
                        WarmupKey = ret,
                        usersCacheCount = cacheSvc.Count(),
                        processorCounter = System.Environment.ProcessorCount,
                        AvailableWorkerThreads = availableWorker,
                        MaxWorkerThreads = maxWorker,
                        OccupiedThreads = maxWorker - availableWorker
                    });
                }
                catch (Exception ex)
                {
                    LogError(ex, ex.Message, null);

                    return Ok(new
                    {
                        error = ex.Message
                    });

                }
            }
            return NotFound();

        }


        [HttpGet]
        [Route("GameInit")]
        public async Task<IActionResult> GameInit()
        {
            if (Request.Headers.ContainsKey("AuthToken"))
            {
                if (Request.Headers["AuthToken"] != ADMIN_AUTH_TOKEN)
                {
                    return NotFound();
                }

                try
                {
                    using (DataSvc da = DataSvc.GetDataService())
                    {
                        await ClearCache(da);
                        await ClearCacheAsync(da);
                    }

                    int usrCount = 0;
                    long sec1 = 0, sec2 = 0;
                    int errors = 0;
                    int totalSize = 0;
                    using (DataSvc da = DataSvc.GetDataService())
                    {
                        var watch = System.Diagnostics.Stopwatch.StartNew();
                        List<User> ls = await da.ExecuteListAsync<User>("select top 10000 * from [User] order by LastActive_At desc", da.Args());
                        if (ls.Count > 0)
                        {
                            usrCount = ls.Count;
                            foreach (User u in ls)
                            {
                                await SetUser(da, u);
                                totalSize += u.AsJson().Length;
                            }
                            watch.Stop();
                            sec1 = watch.ElapsedMilliseconds / 60;
                            watch = System.Diagnostics.Stopwatch.StartNew();
                            foreach (User u in ls)
                            {
                                User test1 = await GetUser(email: u.Email, fromCache: true);
                                if (test1 == null) errors++;
                                User test2 = await GetUser(username: u.Username, fromCache: true);
                                if (test2 == null) errors++;
                                if (u.AuthToken.HasValue)
                                {
                                    User test3 = await GetUser(authToken: u.AuthToken.Value, fromCache: true);
                                    if (test3 == null) errors++;
                                }
                            }
                            watch.Stop();
                            sec2 = watch.ElapsedMilliseconds / 60;

                        }

                    }

                    var gcAfter = GC.GetTotalMemory(false) / 1024 / 1024;
                    Process proc = Process.GetCurrentProcess();
                    var memAfter = proc.WorkingSet64 / 1024 / 1024;
                    var wksetAfter = Environment.WorkingSet / 1024 / 1024;

                    int availableWorker, availableIO;
                    int maxWorker, maxIO;

                    ThreadPool.GetAvailableThreads(out availableWorker, out availableIO);
                    ThreadPool.GetMaxThreads(out maxWorker, out maxIO);

                    return Ok(new
                    {
                        usersCount = usrCount,
                        totalSize = $"{totalSize / 1024 / 1024} Mb",
                        secsToLoad = sec1,
                        secsToParse = sec2,
                        parseErrors = errors,
                        WorkingSetAfter = $"{wksetAfter} Mb",
                        GCAfter = $"{gcAfter} Mb",
                        procAfter = $"{memAfter} Mb",
                        processorCounter = System.Environment.ProcessorCount,
                        AvailableWorkerThreads = availableWorker,
                        MaxWorkerThreads = maxWorker,
                        OccupiedThreads = maxWorker - availableWorker
                    });
                }
                catch (Exception ex)
                {
                    LogError(ex, ex.Message, null);

                    return Ok(new
                    {
                        error = ex.Message
                    });
                }
            }
            return NotFound();
        }


        // GET api/values
        [HttpGet("Test")]
        public async Task<ActionResult<IEnumerable<string>>> Test()
        {
            var ls = new List<string>();

            try
            {
                int availableWorkerThreads;
                int availableAsyncIOThreads;

                ThreadPool.GetAvailableThreads(out availableWorkerThreads, out availableAsyncIOThreads);

                ls.Add($"processorCounter = {System.Environment.ProcessorCount}");
                ls.Add($"AvailableWorkerThreads = {availableWorkerThreads}");
                ls.Add($"availableAsyncIOThreads = {availableAsyncIOThreads}");

                ls.Add($"<strong>Cache:</strong><hr>");

                await Task.Yield();

                int maxWorker, maxIO, availableWorker, availableIO;
                ThreadPool.GetAvailableThreads(out availableWorker, out availableIO);
                ThreadPool.GetMaxThreads(out maxWorker, out maxIO);
                LogInfo($"conn limit:{System.Net.ServicePointManager.DefaultConnectionLimit} - maxWorker:{maxWorker} - maxIO:{maxIO} - availableWorker:{availableWorker} - availableIO:{availableIO}");
                return Ok(ls);  // new string[] { "value1", "value2" };
            }
            catch (Exception ex)
            {
                ls.Add(ex.Message);
                return BadRequest(ls);
            }
        }


        private dynamic GetThreadInfo()
        {
            int availableWorkerThreads;
            int availableAsyncIOThreads;
            ThreadPool.GetAvailableThreads(out availableWorkerThreads, out availableAsyncIOThreads);

            return new { AvailableAsyncIOThreads = availableAsyncIOThreads, AvailableWorkerThreads = availableWorkerThreads };
        }

        [HttpGet]
        [Route("ThreadInfo")]
        public dynamic Get()
        {
            int availableWorker, availableIO;
            int maxWorker, maxIO;

            ThreadPool.GetAvailableThreads(out availableWorker, out availableIO);
            ThreadPool.GetMaxThreads(out maxWorker, out maxIO);

            return new
            {
                AvailableWorkerThreads = availableWorker,
                MaxWorkerThreads = maxWorker,
                OccupiedThreads = maxWorker - availableWorker
            };
        }


        //// GET api/values/5
        //[HttpGet("{id}")]
        //public ActionResult<string> Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/values
        //[HttpPost]
        //public void Post([FromBody] string value)
        //{
        //}

        //// PUT api/values/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE api/values/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
