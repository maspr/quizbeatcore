﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace Core.Common
{
    public static class Strings
    {
        public static bool IsEmpty(this string obj)
        {
            if (obj.IsNull()) return true;
            return (string.IsNullOrEmpty(obj));
        }

        public static bool IsNotEmpty(this string obj)
        {
            if (obj.IsNull()) return false;
            return (!string.IsNullOrEmpty(obj));
        }

        public static bool In(this string val, params string[] list)
        {
            if (val.IsNull()) return false;
            foreach (string x in list)
            {
                if (val == x)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsGUID(this string guid)
        {
            Guid g = new Guid();
            return Guid.TryParse(guid, out g);
        }

        public static bool IsEmptyGUID(this string guid)
        {
            return string.Compare(Guid.Empty.ToString().ToUpper(), guid.AsString().ToUpper(), true) == 0;
        }

        public static bool IsAlphaNum(string str)
        {
            if (string.IsNullOrEmpty(str))
                return false;

            return (str.ToCharArray().All(c => Char.IsLetter(c) || Char.IsNumber(c)));
        }

        public static bool IsNum(string str)
        {
            if (string.IsNullOrEmpty(str))
                return false;
            return (str.ToCharArray().All(c => Char.IsNumber(c)));
        }

        #region < Join String Array >
        public static string Join(this string[] obj, string separator = ",")
        {
            string ret = string.Empty;
            obj.ForEach(x => ret += separator + x);
            return ret.TrimStart(separator[0]).TrimEnd(separator[0]);
        }

        public static string Join(this List<string> obj, string separator = ",")
        {
            string ret = string.Empty;
            obj.ForEach(x => ret += separator + x);
            return ret.TrimStart(separator[0]).TrimEnd(separator[0]);
        }
        #endregion

        public static string ToHex(this byte[] data)
        {
            if (data == null)
            {
                return "";
            }
            return BitConverter.ToString(data).Replace("-", "").ToLower();
        }


        /// <summary>
        /// String -> To Camel Case
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string ToCamelCase(this string s)
        {
            if (s != string.Empty && char.IsUpper(s[0]))
            {
                s = char.ToLower(s[0]) + s.Substring(1);
            }

            return s;
        }

        /// <summary>
        /// Removes CRLFs and returns a flat string
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string Flatten(this string str)
        {
            if (str.IsNullOrEmpty()) return string.Empty;
            return str.AsString().Replace(Environment.NewLine, " ").Trim();
        }

        public static string AddLine(this string str, string line)
        {
            if (str.IsNullOrEmpty()) str = string.Empty;
            if (!str.IsNullOrEmpty()) str += Environment.NewLine;
            str += line;
            return str;
        }

        /// <summary>
        /// Uses TypeConverter and Convert.ChangeType to cast &lt;obj&gt; to &lt;T&gt; 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T As<T>(this string obj)
        {
            if (obj.IsNull()) return default(T);
            TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));
            if (converter != null)
            {
                return (T)converter.ConvertFrom(obj);
            }
            else
            {
                return (T)Convert.ChangeType(obj, typeof(T));
            }
        }

        public static string Replace(this string input, string search, string replace, bool ignoreCase)
        {
            string retVal = input;
            if (ignoreCase == true)
            {
                retVal = System.Text.RegularExpressions.Regex.Replace(input, search, replace, RegexOptions.IgnoreCase | RegexOptions.Multiline);
            }
            else
            {
                retVal = input.Replace(search, replace);
            }
            return retVal;
        }

    }
}
