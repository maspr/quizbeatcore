﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Common
{
    public static class ConfigurationExtentions
    {
        /// <summary>
        /// AsDictionary
        /// </summary>
        /// <param name="config"></param>
        /// <param name="jsonSectionPath">Json appsettings section path, i.e. "AppSettings:ConnectionStrings"</param>
        /// <returns></returns>
        public static Dictionary<string,string> AsDictionary(this IConfiguration config, string jsonSectionPath)
        {
            return config.GetSection(jsonSectionPath).GetChildren().ToDictionary(x => x.Key, x => x.Value);
        }

    }
}
