﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;
using System.Collections;
using System.Data;
using System.Globalization;
using System.Reflection;

namespace Core.Common
{
    public static class Object
    {

        public static T CopyTo<T>(this object source) where T : new()
        {
            T dest = Activator.CreateInstance<T>();
            PropertyInfo[] destProps = dest.GetType().GetProperties();
            PropertyInfo[] thisProps = source.GetType().GetProperties();

            foreach (PropertyInfo property in thisProps)
            {
                string name = property.Name;
                if (name == "Item") continue;
                object value = property.GetValue(source, null);
                PropertyInfo p = destProps.FirstOrDefault(x => x.Name == property.Name);
                if (p != null) p.SetValue(dest, value);
            }
            return dest;
        }


        public static T CopyTo<T>(this object source, T dest = default(T)) where T : new()
        {
            if (dest == null) dest = Activator.CreateInstance<T>();
            PropertyInfo[] destProps = dest.GetType().GetProperties();
            PropertyInfo[] thisProps = source.GetType().GetProperties();

            foreach (PropertyInfo property in thisProps)
            {
                string name = property.Name;
                if (name == "Item") continue;
                object value = property.GetValue(source, null);
                PropertyInfo p = destProps.FirstOrDefault(x => x.Name == property.Name);
                if (p != null) p.SetValue(dest, value);
            }
            return dest;
        }

        public static bool IsEqual(this object obj1, object obj2)
        {
            return CompareEqual(obj1, obj2);
        }

        static public bool CompareEqual(object obj1, object obj2)
        {
            if (IsNull(obj1) && IsNull(obj2)) return true;
            if (IsNull(obj1) || IsNull(obj2)) return false;
            if (obj1 is byte[] && obj2 is byte[])
                return ((byte[])obj1).SequenceEqual((byte[])obj2);
            if (IsZeroEqual(obj1) && IsZeroEqual(obj2)) return true;
            return obj1.Equals(obj2);
        }

        /// <summary>
        /// Returns true if object value is null or DBNull
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool IsNull(this object obj)
        {
            return obj == null || obj == DBNull.Value || Convert.IsDBNull(obj) == true;
        }

        private static bool IsZeroEqual(object obj)
        {
            if (obj is decimal) return Convert.ToDecimal(obj) == decimal.Zero;
            if (obj is Int32 || obj is Int16 || obj is Int64) return Convert.ToInt64(obj) == 0;
            return false;

        }

        static public bool IsGuid(object value)
        {
            if (value is System.Guid)
                return true;
            Regex GuidRegEx = new Regex(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", RegexOptions.Compiled);
            if (value is string && !string.IsNullOrEmpty(value.ToString()) && GuidRegEx.IsMatch(value.ToString()))
                return true;
            return false;
        }

        public static bool IsNumeric(this object Expression)
        {
            bool isNum;
            double retNum;
            if (Expression == null) return false;
            isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        public static decimal Round(this decimal val, int decimals)
        {
            return Math.Round(val, decimals);
        }

        public static decimal Round(this decimal? val, int decimals)
        {
            if (val == null) return 0;
            if (val.IsNull()) return 0;
            return Math.Round(val.Value, decimals);
        }

        #region < IsNullOrEmpty() >
        public static bool IsNullOrEmpty(this object obj)
        {
            if (obj.IsNull()) return true;
            if (obj is IList)
            {
                if ((obj as IList).Count == 0) return true;
            }
            if (obj is ICollection)
            {
                if ((obj as ICollection).Count == 0) return true;
            }
            return (obj.AsString() == string.Empty);
        }

        public static T IsNullOrEmpty<T>(this T obj, T defaultValue)
        {
            if (obj.IsNull()) return defaultValue;
            if (obj is IList<T>)
            {
                if ((obj as IList<T>).Count == 0) return defaultValue;
            }
            else if (obj is ICollection<T>)
            {
                if ((obj as ICollection<T>).Count == 0) return defaultValue;
            }
            if (obj is IList)
            {
                if ((obj as IList).Count == 0) return defaultValue;
            }
            else if (obj is ICollection)
            {
                if ((obj as ICollection).Count == 0) return defaultValue;
            }
            return (obj.AsString() == string.Empty ? defaultValue : obj);
        }
        #endregion

        /// <summary>
        /// Returns object value as integer or 0
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static int AsInt(this object obj)
        {
            if (obj.IsNull()) return 0;
            return Convert.ToInt32(obj);
        }

        public static decimal AsDecimal(this object obj)
        {
            return (obj.IsNull() || obj == DBNull.Value || string.IsNullOrEmpty(obj.AsString().Trim())) ? 0 : Convert.ToDecimal(obj);
        }

        public static double AsDouble(this object obj)
        {
            return (obj.IsNull() || obj == DBNull.Value || string.IsNullOrEmpty(obj.AsString().Trim())) ? 0 : Convert.ToDouble(obj);
        }

        public static string AsJsList(this IList obj)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var x in obj)
            {
                sb.Append($"{x},");
            }
            return sb.ToString().TrimEnd(',');
        }

        /// <summary>
        /// Returns object value as string or string.empty
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string AsString(this object obj)
        {
            return obj.IsNull() ? string.Empty : $"{obj}";
        }

        public static string AsString(this DataRow obj, string columnName)
        {
            if (obj.IsNull()) return string.Empty;
            if (obj.IsNull(columnName)) return string.Empty;

            return obj[columnName].ToString();
        }

        public static string AsSqlNumber(this object n)
        {
            double d = System.Convert.ToDouble(n);
            return d.ToString("N2", CultureInfo.CreateSpecificCulture("el-GR")).Replace(".", "").Replace(",", ".");
        }

        public static bool In<T>(this T val, params T[] list) where T : struct
        {
            //var y = list.FirstOrDefault<T>(x => val.Equals(x) || EqualityComparer<T>.Default.Equals(val, x));
            if (val.IsNull()) return false;
            foreach (T x in list)
            {
                if (val.Equals(x) || EqualityComparer<T>.Default.Equals(val, x))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool In<T>(this T val, List<T> values) where T : struct
        {
            if (val.IsNull()) return false;
            if (typeof(T).GetTypeInfo().IsEnum)
            {
                return values.Contains(val);
            }
            return values.Contains(val);
        }

        /// <summary>
        /// Tries to cast object to type of T. 
        /// If the internal value is null or the cast is wrond, it returns null otherwise
        /// it returns type of T object or default(T)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T As<T>(this object obj) where T : class
        {
            if (obj.IsNull()) return null;

            if (obj is T)
                return (T)obj;
            else
                return null;
        }


        /// <summary>
        /// Cast object to type T. If not applicable return defaultValue of T. 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static T As<T>(this object obj, T defaultValue)
        {
            if (obj.IsNull()) return defaultValue;

            if (obj is T)
                return (T)obj;
            else
                return defaultValue;
        }


        /// <summary>
        /// 	If target is null, returns null.
        /// 	Othervise returns string representation of target using specified format provider.
        /// </summary>
        /// <param name = "target">Target transforming to string representation. Can be null.</param>
        /// <param name = "formatProvider">Format provider used to transformation target to string representation.</param>
        /// <example>
        /// 	CultureInfo czech = new CultureInfo("cs-CZ");
        /// 
        /// 	float? number = null;
        /// 	string text1 = number.AsString( czech );
        /// 
        /// 	number = 15.7892;
        /// 	string text2 = number.AsString( czech );
        /// </example>
        /// <remarks>
        /// </remarks>
        public static string AsString(this object target, IFormatProvider formatProvider)
        {
            var result = string.Format(formatProvider, "{0}", target);
            return result;
        }

        public static DateTime? AsDateTime(this DataRow obj, string columnName)
        {
            if (obj.IsNull()) return null;
            if (obj.IsNull(columnName)) return null;

            return obj[columnName] as DateTime?;
        }

        public static DateTime? AsDateTime(this object obj)
        {
            if (obj.IsNull()) return null;

            return obj as DateTime?;
        }

        public static T AsValue<T>(this object obj) where T : struct
        {
            if (obj.IsNull()) return default(T);

            if (obj is T)
                return (T)obj;
            else
                return default(T);  // default(T);
        }
        public static T? AsNullableValue<T>(this object obj) where T : struct
        {
            if (obj.IsNull()) return null;

            if (obj is T)
                return (T)obj;
            else
                return null;
        }



        /// <summary>
        ///     An EventHandler extension method that raises the event event.
        /// </summary>
        /// <param name="this">The @this to act on.</param>
        /// <param name="sender">Source of the event.</param>
        public static void RaiseEvent(this EventHandler @this, object sender)
        {
            if (@this != null)
            {
                @this(sender, null);
            }
        }

        /// <summary>
        ///     An EventHandler extension method that raises.
        /// </summary>
        /// <param name="handler">The handler to act on.</param>
        /// <param name="sender">Source of the event.</param>
        /// <param name="e">Event information.</param>
        public static void Raise(this EventHandler handler, object sender, EventArgs e)
        {
            if (handler != null)
                handler(sender, e);
        }

        /// <summary>
        ///     An EventHandler&lt;TEventArgs&gt; extension method that raises the event event.
        /// </summary>
        /// <typeparam name="TEventArgs">Type of the event arguments.</typeparam>
        /// <param name="this">The @this to act on.</param>
        /// <param name="sender">Source of the event.</param>
        public static void RaiseEvent<TEventArgs>(this EventHandler<TEventArgs> @this, object sender) where TEventArgs : EventArgs
        {
            if (@this != null)
            {
                @this(sender, Activator.CreateInstance<TEventArgs>());
            }
        }

        /// <summary>
        ///     An EventHandler&lt;TEventArgs&gt; extension method that raises the event event.
        /// </summary>
        /// <typeparam name="TEventArgs">Type of the event arguments.</typeparam>
        /// <param name="this">The @this to act on.</param>
        /// <param name="sender">Source of the event.</param>
        /// <param name="e">Event information to send to registered event handlers.</param>
        public static void RaiseEvent<TEventArgs>(this EventHandler<TEventArgs> @this, object sender, TEventArgs e) where TEventArgs : EventArgs
        {
            if (@this != null)
            {
                @this(sender, e);
            }
        }


    }
}
