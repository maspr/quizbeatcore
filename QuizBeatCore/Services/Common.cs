﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;

namespace QuizBeatBusiness
{
    public static class Common
    {

        public const string Cookie_AUTHENTICATIONTOKEN = "AuthenticationToken";
        public const string Cookie_USR = "usr";
        public const string Cookie_AUTHORIZATION = "Authorization";


        public static string asJsnum(this decimal num, int decimals = 2)
        {
            return num.ToString($"N{decimals}").Replace(",", ".");
        }

        public static void LogException(this ILogger Logger, Exception ex)
        {
            Logger.LogError(new EventId(0) { }, ex, ex.Message, new object[] { });
        }


        public static Newtonsoft.Json.JsonSerializerSettings JsonSerializerSettings()
        {
            return new Newtonsoft.Json.JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore,
                Formatting = Formatting.Indented,
                MissingMemberHandling = MissingMemberHandling.Ignore,
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                Culture = System.Globalization.CultureInfo.InvariantCulture,
                DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.Unspecified,
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.None
            };
        }


    }

}
