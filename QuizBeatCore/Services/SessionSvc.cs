﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using QuizBeatBusiness.Models;
using Core.Common;
using Core.Common.Login;
using Core.Common.Session;
using System;
using System.Threading.Tasks;

namespace QuizBeatBusiness.Services
{
    /*
      http://asp.net-hacker.rocks/2016/02/17/dependency-injection-in-aspnetcore.html
      services.AddTransient<ICountryService, CountryService>();            
      @inject QuizBeatBusiness.Services.ICountryService CountryService;

       @if (countryService.All().Any()) { 
            <ul> 
                @foreach (var country in CountryService.All().OrderBy(x => x.Name)) 
                { 
                        <p>@country.Name (@country.Code)</p> 
                } 
            </ul> 
        }
      
     * */
    public class SessionService : ISessionService
    {

        private readonly IHttpContextAccessor contextAccessor;
        private readonly IMemoryCache MemCache;

        public SessionService(IHttpContextAccessor contextAccessor, IMemoryCache cache)
        {
            this.contextAccessor = contextAccessor;
            this.MemCache = cache;
        }

        public HttpContext GetContext()
        {
            return contextAccessor.HttpContext;
        }


        public User AuthenticateUser()
        {
            return AuthenticateUserAsync().GetAwaiter().GetResult();
        }
        public async Task<User> AuthenticateUserAsync()
        {
            User usr = null;
            var cookie = GetContext().Request.Cookies[QuizBeatBusiness.Common.Cookie_AUTHENTICATIONTOKEN];
            if (cookie.IsNullOrEmpty()) cookie = GetContext().Request.Headers[QuizBeatBusiness.Common.Cookie_AUTHENTICATIONTOKEN];
            if (cookie.IsNullOrEmpty()) cookie = GetContext().Request.Headers[QuizBeatBusiness.Common.Cookie_AUTHORIZATION];

            if (!cookie.IsNullOrEmpty())
            {
                if (!MemCache.TryGetValue(cookie, out usr))
                {
                    try
                    {
                        using (DataSvc da = DataSvc.GetDataService())
                        {
                            usr = await da.GetSessionUserAsync(cookie);
                            if (usr == null) return null;
                            if (usr.Role == enRoleType.Customer)
                            {
                                //
                                //
                                //
                            }
                            MemCache.Set(cookie, usr);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            return usr;
        }


        public void PutUserSession(User usr)
        {
            GetContext().Session.SetString(QuizBeatBusiness.Common.Cookie_AUTHENTICATIONTOKEN, usr.AuthenticationToken);
            GetContext().Session.SetString(QuizBeatBusiness.Common.Cookie_USR, usr.AsJson());
            try
            {
                using (DataSvc da = DataSvc.GetDataService())
                {
                    Session s = da.GetSession(usr.ID);
                    s.AddAttribute(QuizBeatBusiness.Common.Cookie_USR, usr.AsJson(), usr.AuthenticationToken);
                    da.UpdateSession(s);
                }
            }
            catch (Exception) { }
            CookieOptions options = new CookieOptions() { Expires = DateTime.Now.AddDays(7) };
            GetContext().Response.Cookies.Append(QuizBeatBusiness.Common.Cookie_USR, usr.AsJson(), options);
        }

        /// <summary>
        /// Returns current User object from ASP.Session object
        /// </summary>
        /// <returns></returns>
        public User GetUser()
        {
            //HttpContext context = GetContext();
            //if (context == null) return null;
            //if (context.Session == null) return null;

            //string json = context.Session.GetString(QuizBeatBusiness.Common.Cookie_USR);
            //if (!json.IsNullOrEmpty())
            //{
            //    User user = json.FromJson<User>();
            //    return user;
            //}
            return AuthenticateUser();
        }

        public void ClearCookies()
        {
            HttpContext ctx = GetContext();
            if (ctx != null)
            {
                var c = GetContext().Request.Cookies[QuizBeatBusiness.Common.Cookie_AUTHENTICATIONTOKEN];
                if (!c.IsNullOrEmpty())
                {
                    MemCache.Remove(c);
                }
                foreach (string cookie in ctx.Request.Cookies.Keys)
                {
                    ctx.Response.Cookies.Append(cookie, "", new CookieOptions() { Expires = DateTime.Now.AddDays(-1) });
                    ctx.Response.Cookies.Delete(cookie);
                }
            }

        }

        public Session GetUserSession()
        {
            HttpContext context = GetContext();
            if (context == null) return null;
            if (context.Session == null) return null;
            User user = GetUser();
            if (user != null)
            {
                return GetUserSession(user.ID);
            }
            return null;
        }

        public Session GetUserSession(string userID)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return da.GetSession(userID);
            }
        }

        public void UpdateUserSession(Session s)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                da.UpdateSession(s);
            }
        }

        public void ClearSession()
        {
            var cookie = GetContext().Request.Cookies[QuizBeatBusiness.Common.Cookie_AUTHENTICATIONTOKEN];
            if (!cookie.IsNullOrEmpty())
            {
                using (DataSvc da = DataSvc.GetDataService())
                {
                    da.ClearSession(cookie);
                }
            }
            ClearCookies();
        }

    }


    public interface ISessionService
    {
        /// <summary>
        /// Returns current User object from ASP.Session object
        /// </summary>
        /// <returns></returns>
        User GetUser();

        /// <summary>
        /// Returns current Session object from ASP.Session object
        /// </summary>
        /// <returns></returns>
        Session GetUserSession();

        Session GetUserSession(string userID);

        void UpdateUserSession(Session s);

        HttpContext GetContext();

        void ClearCookies();

        void ClearSession();

        User AuthenticateUser();

        Task<User> AuthenticateUserAsync();

        void PutUserSession(User usr);
    }


}

