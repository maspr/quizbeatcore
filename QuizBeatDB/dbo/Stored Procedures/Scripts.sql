﻿insert into Cache (id, [Key], Value, DateCreated)
SELECT NEWID() as id, 'USR_' + u.Username as [Key],
(
	SELECT TOP 1 
		[Email] as email, 
		[Password] as password, 
		[Username] as username, 
		CAST([Money_Won] as decimal(6,2)) as money_Won, 
		[ExtraLives] as extraLives, 
		[AuthToken] authToken, 
		[PushToken] pushToken, 
		[id], 
		CAST([Money_CashedOut] as decimal(6,2)) as money_CashedOut, 
		[Created_At] as created_At, 
		[IsApproved] as isApproved, 
		[DeviceType] as deviceType, 
		[LastActive_At] as lastActive_At, 
		[Ranking] as ranking
	FROM [User] WHERE id=u.ID FOR JSON AUTO
) as Value,
GETDATE() as DateCreated
FROM [USER] u order by UserName


/*

TABLE
	IPS
		id
		index 
		IP
		TYPE (stream/game)

User
	SrtreamingURL
	GameServerURL

GameInit()
Update user table with random IPs
rand(1~6) -> 5

join user + IPs on IDX -> IP

Mobile getProfile() get IP + enjoy!!!

*/