﻿CREATE TABLE [dbo].[Subscription](
	[ID] [varchar](40) NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[UserID] [varchar](40) NULL,
	[Receipt] [varchar](40) NULL,
	[DeviceType] [bit] NULL,
	[HasBeenRefunded] [bit] NULL,
PRIMARY KEY CLUSTERED([ID] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]
GO

CREATE INDEX [IX_UserID] ON [dbo].[Subscription] ([UserID],[StartDate],[EndDate])
GO

CREATE INDEX [IX_UserID_Start_End] ON [dbo].[Subscription] ([UserID])
GO
