﻿CREATE TABLE [dbo].[User] (
    [ID]                         VARCHAR(40)  NOT NULL,
    [Username]                   NVARCHAR (40)     COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,  /*COLLATE SQL_Latin1_General_CP1_CI_AS */
    [Password]                   NVARCHAR (250)    COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,  /*COLLATE SQL_Latin1_General_CP1_CI_AS */
    [Email]                      VARCHAR (40)     COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,  /*COLLATE SQL_Latin1_General_CP1_CI_AS */
    [Money_Won]                  FLOAT (53)        CONSTRAINT [DF__User__Money_Won] DEFAULT ((0)) NOT NULL,
    [ExtraLives]                 INT               CONSTRAINT [DF__User__ExtraLives] DEFAULT ((0)) NOT NULL,
    [AuthToken]                  VARCHAR(40)  NULL,
    [PushToken]                  VARCHAR (250)     NULL,
    [ResetPasswordToken]         VARCHAR (250)     NULL,
    [ResetPasswordRequestedDate] DATETIME          NULL,
    [Money_CashedOut]            FLOAT (53)        CONSTRAINT [DF__User__Money_Cash] DEFAULT ((0)) NOT NULL,
    [Created_At]                 DATETIME          NULL,
    [IsApproved]                 BIT               CONSTRAINT [DF__User__IsApproved] DEFAULT ((1)) NOT NULL,
    [DeviceType]                 INT               CONSTRAINT [DF__User__DeviceType] DEFAULT ((0)) NULL,
    [OneSignalID]                VARCHAR (250)     NULL,
    [LastActive_At]              DATETIME          NULL,
    [Ranking]                    INT               CONSTRAINT [DF_User_Ranking] DEFAULT (0) NULL,
    [HasSharedAppOnFacebook]     BIT               NULL,
    [HasSharedAppOnTwitter]      BIT               NULL,
	[IsQuizMaster]				 BIT			   NULL,
    CONSTRAINT [PK__User] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MONEYWON]
    ON [dbo].[User]([Money_Won] DESC);


GO
CREATE NONCLUSTERED INDEX [IX_AUTHTOKEN]
    ON [dbo].[User]([AuthToken] ASC);

GO

CREATE INDEX [IX_User_Username_Password] ON [dbo].[User] ([Username] ASC, [Password] ASC)

GO
CREATE INDEX [IX_User_Ranking] ON [dbo].[User] ([Ranking] ASC) INCLUDE ([Username], [Email], [Money_Won])

--GO
--CREATE UNIQUE INDEX [IX_User_Email] ON [dbo].[User] ([Email])
--GO
--CREATE UNIQUE INDEX [IX_User_UserName] ON [dbo].[User] ([UserName])
