﻿CREATE TABLE [dbo].[User_Game] (
    [ID]						VARCHAR(40) NOT NULL,
    [UserID]					VARCHAR(40) NULL,
    [GameID]					VARCHAR(40) NULL,
    [HasUsedExtraLive]			BIT         CONSTRAINT [DF__User_Game__HasUsedExtraLive] DEFAULT ((0)) NOT NULL,
    [HasBeenEliminated]			BIT         CONSTRAINT [DF__User_Game__HasBeenEliminated] DEFAULT ((0)) NOT NULL,
    [DateJoined]				DATETIME    NULL,
    [HasBeenBanned]				BIT         CONSTRAINT [DF_User_Game_HasBeenBanned] DEFAULT ((0)) NULL,
    [HasUsedEraserHelp]			BIT          NULL,
    [HasUsedAudienceHelp]		BIT			CONSTRAINT [DF_User_Game_HasUsedAudienceHelp] DEFAULT ((0)) NULL,
    CONSTRAINT [PK__User_Game] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [User_Game_Game] FOREIGN KEY ([GameID]) REFERENCES [dbo].[Game] ([ID]),
    CONSTRAINT [User_Game_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([ID])
);


GO
CREATE INDEX [IX_USERID_GAMEID] ON [dbo].[User_Game]([UserID] ASC, [GameID] ASC);


GO

CREATE INDEX [IX_User_Game_UserID] ON [dbo].[User_Game] ([UserID] ASC)

GO

CREATE INDEX [IX_User_Game_GameID] ON [dbo].[User_Game] ([GameID] ASC)

GO