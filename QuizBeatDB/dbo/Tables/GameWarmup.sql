﻿CREATE TABLE [dbo].[GameWarmup] (
    [ID]			VARCHAR(40) NOT NULL,
    [Key]			INT NOT NULL,
    [Value]			NVARCHAR (MAX)	 NULL,
    CONSTRAINT [PK__GameWarmup] PRIMARY KEY CLUSTERED ([ID] ASC),
);
GO

CREATE INDEX [IX_GameWarmup_Key] ON [dbo].[GameWarmup] ([Key])
GO
