﻿CREATE TABLE [dbo].[User_Question] (
    [ID]               VARCHAR(40)	NOT NULL,
    [UserID]           VARCHAR(40)	NULL,
    [QuestionID]       VARCHAR(40)	NULL,
    [AnswerGiven]      INT			NULL,
    [IsCorrect]        BIT			NULL,
    [User_GameID]      VARCHAR(40)	NULL,
    [GameID]           VARCHAR(40)	NULL,
    [HasUsedExtraLive] BIT          CONSTRAINT [DF__User_Question__HasUsedExtraLive] DEFAULT ((0)) NULL,
    [DateCreated]      DATETIME     CONSTRAINT [DF__User_Question__DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK__User_Question] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [User_Game_User_Question] FOREIGN KEY ([User_GameID]) REFERENCES [dbo].[User_Game] ([ID]),
    CONSTRAINT [User_Question_Question] FOREIGN KEY ([QuestionID]) REFERENCES [dbo].[Question] ([ID]),
    CONSTRAINT [User_Question_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([ID])
);

GO
CREATE NONCLUSTERED INDEX [IX_User_Question_GameID]
    ON [dbo].[User_Question]([User_GameID] ASC)
    INCLUDE([AnswerGiven], [GameID], [HasUsedExtraLive], [IsCorrect], [QuestionID], [UserID]);

GO
CREATE NONCLUSTERED INDEX [IX_USERID_QUESTIONID] ON [dbo].[User_Question]([UserID] ASC, [QuestionID] ASC);

GO
CREATE NONCLUSTERED INDEX [IX_QUESTIONID] ON [dbo].[User_Question]([QuestionID] ASC);

GO
CREATE NONCLUSTERED INDEX [IX_QUESTIONID_ISCORRECT] ON [dbo].[User_Question]([UserID] ASC, [QuestionID] ASC, [IsCorrect] ASC);

GO
CREATE NONCLUSTERED INDEX [IX_User_Question_GameID_User_GameID] ON [dbo].[User_Question]([GameID] ASC, [User_GameID] ASC);
GO
CREATE NONCLUSTERED INDEX IX_User_Question_QuestionID_AnswerGiven ON [dbo].[User_Question] ([QuestionID],[AnswerGiven])
GO
