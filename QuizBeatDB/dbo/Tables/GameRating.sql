﻿CREATE TABLE [dbo].[GameRating] (
    [ID]     VARCHAR(40) NOT NULL,
    [GameID] VARCHAR(40) NULL,
    [UserID] VARCHAR(40) NULL,
    [Rating] INT           NULL,
    CONSTRAINT [PK__GameRating] PRIMARY KEY CLUSTERED ([ID] ASC),
);

GO
CREATE INDEX [IX_GameRating_UserID] ON [dbo].[GameRating] ([UserID] ASC)
GO
CREATE INDEX [IX_GameRating_GameID] ON [dbo].[GameRating] ([GameID] ASC)
GO
CREATE INDEX [IX_GameRating_GameID_UserID] ON [dbo].[GameRating] ([GameID] ASC,[UserID] ASC)
