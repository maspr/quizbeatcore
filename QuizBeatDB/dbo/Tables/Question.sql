﻿CREATE TABLE [dbo].[Question] (
    [ID]             VARCHAR(40)  NOT NULL,
    [GameID]         VARCHAR(40)  NOT NULL,
    [Text]           NVARCHAR (MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Answer_1]       NVARCHAR (MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Answer_2]       NVARCHAR (MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Answer_3]       NVARCHAR (MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [CorrectAnswer]  INT            NULL,
    [Sorting]        INT            NULL,
    [TimeWentLive]   DATETIME       NULL,
    [HasAudio]       BIT            CONSTRAINT [DF__Question__HasAudio] DEFAULT ((0)) NOT NULL,
    [ImageUrlString] VARCHAR (250)  NULL,
    CONSTRAINT [PK__Question] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [Game_Question] FOREIGN KEY ([GameID]) REFERENCES [dbo].[Game] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [IX_GAMEID]
    ON [dbo].[Question]([GameID] ASC);
GO
CREATE NONCLUSTERED INDEX [IX_GAMEID_Sorting]
    ON [dbo].[Question]([GameID] ASC, [Sorting] ASC);

