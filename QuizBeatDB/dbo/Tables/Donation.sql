﻿CREATE TABLE [dbo].[Donation] (
    [ID]           VARCHAR(40) NOT NULL,
    [Text]         NVARCHAR (MAX) NULL,
    [MoneyTarget]  FLOAT (53)     NULL,
    [MoneyReached] FLOAT (53)     NULL,
    [Company]      NVARCHAR (255) NULL,
    [IsActive]     BIT            NULL,
    CONSTRAINT [PK__Donation] PRIMARY KEY CLUSTERED ([ID] ASC),
);


GO
CREATE NONCLUSTERED INDEX [Donation_Active]
    ON [dbo].[Donation]([IsActive] ASC);

