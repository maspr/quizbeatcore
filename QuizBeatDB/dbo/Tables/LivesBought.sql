﻿CREATE TABLE [dbo].[LivesBought] (
    [ID]            VARCHAR(40)  NOT NULL,
    [DateCreated]   DATETIME       NULL,
    [NumberOfLives] INT            NULL,
    [UserID]        VARCHAR(40)  NULL,
    [ReceiptData]   NVARCHAR (MAX) NULL,
    [TransactionID] VARCHAR(300) NULL, 
    CONSTRAINT [PK__LivesBought] PRIMARY KEY CLUSTERED ([ID] ASC)
);
GO
CREATE INDEX [IX_LivesBought_UserID] ON [dbo].[LivesBought] ([UserID] ASC)
GO
CREATE INDEX [IX_LivesBought_UserID_LivesBought] ON [dbo].[LivesBought] ([UserID] ASC, [NumberOfLives] ASC)
GO
